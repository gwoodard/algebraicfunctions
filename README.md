# Algebraic Functions #

Homework assignment for Design of Analysis of Algorithm class based on solving different algebraic functions using N as a independent variable. N is the number of data. Display the results in a tabular format. The range of N will be 5, 10, 50, 100, 200, 500, 1000. Some of the values may go out of bounds. The functions are below:

* **F1** - lgN
* **F2** - square root
* **F3** - N
* **F4** - NlogN
* **F5** - N^2
* **F6** - 2^N
* **F7** - N!
* **F8** - (1.05)^N


**Language:** C++