/*
George Woodard
Homework 2, Question 5
*/


#include <iostream>
#include <math.h> 

using namespace std;

int factorial(int n)

{
	if(n==1)
	{
		return 1;
	}
	else
	{
		int value = n * factorial(n-1);
		cout<<" " <<value<<",";
		return value;
	}
}


int main()
{
	double x;
	cout<<"Enter a value: ";
	cin>>x;
	cout<<"The lgN of "<< x <<" is: "<<log(x)<<endl;
	cout<<"The square root of "<< x <<" is: "<<sqrt(x)<<endl;
	cout<<"The NlogN of "<< x <<" is: "<<x*(log10(x))<<endl;
	cout<<"The N^2 of "<< x <<" is: "<<pow(x, 2)<<endl;
	cout<<"The 2^N of "<< x <<" is: "<<pow(2, x)<<endl;
	cout<<"The (1.05)^N  of "<< x <<" is: "<<pow(1.05, x)<<endl;
	cout<<"The N! of "<< x <<" is: "<<factorial(x)<<endl;

	return 0;

}

